import logo from "./logo.svg";
import "./App.css";
import ShoesShop_Redux from "./ShoesShop-Redux/ShoesShop_Redux";

function App() {
  return (
    <div className="App">
      <ShoesShop_Redux />
    </div>
  );
}

export default App;
