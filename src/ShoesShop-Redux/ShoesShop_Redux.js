import React, { Component } from "react";
import Controller from "./Controller";
import ProductList from "./ProductList";
import Model from "./Model";
import CartShoes from "./CartShoes";

class ShoesShop_Redux extends Component {
  render() {
    return (
      <div>
        <header className="display-4 p-3 text-center">Shoes Shop</header>
        <div className="container-fluid row">
          <Controller />
          <div class="col-9">
            <div class="tab-content" id="nav-tabContent">
              <div
                class="tab-pane fade show active"
                id="list-home"
                role="tabpanel"
                aria-labelledby="list-home-list"
              >
                <ProductList />
                <Model />
              </div>
              <div
                class="tab-pane fade"
                id="list-shop"
                role="tabpanel"
                aria-labelledby="list-shop-list"
              >
                <CartShoes />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoesShop_Redux;
