import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, CHANGE_DETAIL } from "./constantShoes/constantShoes";

class ProductItem extends Component {
  render() {
    return (
      <div className="col-lg-4 col-md-6 col-sm-12 p-2">
        <div className="card text-left">
          <img className="card-img-top" src={this.props.shoes.image} alt />
          <div className="card-body">
            <h6 className="card-title">{this.props.shoes.name}</h6>
            <p className="card-text m-0">{this.props.shoes.price} $</p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.shoes);
              }}
              className="btn btn-dark rounded-0"
            >
              Add to carts
            </button>
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.shoes);
              }}
              className="btn btn-dark rounded-0 ml-4"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeDetail: (shoes) => {
      let action = {
        type: CHANGE_DETAIL,
        payload: shoes,
      };
      return dispatch(action);
    },
    handleAddToCart: (shoes) => {
      let action = {
        type: ADD_TO_CART,
        payload: shoes,
      };
      return dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ProductItem);
