import React, { Component } from "react";
import { connect } from "react-redux";

class Model extends Component {
  render() {
    return (
      <div className="row pt-5">
        <img src={this.props.detail.image} className="col-4" alt="" />
        <div className="col-8 text-left">
          <p>Name: {this.props.detail.name}</p>
          <p>Price: {this.props.detail.price} $</p>
          <p>Desciption: {this.props.detail.description}</p>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.shoesReducer.detail,
  };
};
export default connect(mapStateToProps)(Model);
