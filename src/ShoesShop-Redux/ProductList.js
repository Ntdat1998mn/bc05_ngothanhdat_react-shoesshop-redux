import React, { Component } from "react";
import ProductItem from "./ProductItem";
import { connect } from "react-redux";

class ProductList extends Component {
  renderProductList = () => {
    var result = this.props.list.map((item) => {
      return <ProductItem shoes={item} />;
    });
    return result;
  };

  render() {
    return <div className="row">{this.renderProductList()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoesReducer.shoesArr,
  };
};
export default connect(mapStateToProps)(ProductList);
