import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD, REMOVE, SUB } from "./constantShoes/constantShoes";

class CartShoe extends Component {
  renderTbody = () => {
    return this.props.listCart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td className="text-left">{item.name}</td>
          <td>{item.price} $</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item, SUB);
              }}
              className="btn btn-danger p-0 mx-2"
              style={{
                width: "20px",
                height: "20px",
                lineHeight: "20px",
              }}
            >
              -
            </button>

            <span>{item.quantity}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item, ADD);
              }}
              className="btn btn-danger p-0 mx-2"
              style={{
                width: "20px",
                height: "20px",
                lineHeight: "20px",
              }}
            >
              +
            </button>
          </td>
          <td
            style={{
              height: "60px",
            }}
          >
            <img
              style={{
                width: "70px",
                height: "30px",
                objectFit: "cover",
              }}
              src={item.image}
              alt=""
            />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              className="btn-danger"
            >
              Remove
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="row p-2">
        <table className="table">
          <thead className="border">
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Picture</th>
            <th>Remove</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    listCart: state.shoesReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuantity: (shoes, method) => {
      let action = {
        type: method,
        payload: shoes,
      };
      return dispatch(action);
    },
    handleRemove: (idRemove) => {
      let action = {
        type: REMOVE,
        payload: idRemove,
      };
      return dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
