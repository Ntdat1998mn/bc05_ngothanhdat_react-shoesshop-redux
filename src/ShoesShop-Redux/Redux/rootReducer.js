import { shoesReducer } from "./shoesReducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({ shoesReducer });
