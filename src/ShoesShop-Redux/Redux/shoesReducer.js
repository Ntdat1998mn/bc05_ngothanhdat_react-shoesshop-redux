import {
  ADD,
  ADD_TO_CART,
  CHANGE_DETAIL,
  REMOVE,
  SUB,
} from "../constantShoes/constantShoes";
import { dataShoes } from "../dataShoes";

let initialState = {
  shoesArr: dataShoes,
  detail: dataShoes[0],
  cart: [],
};

export const shoesReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_DETAIL: {
      state.detail = action.payload;
      return { ...state };
    }
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let cartItem = { ...action.payload, quantity: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].quantity++;
      }
      return { ...state, cart: cloneCart };
    }
    case ADD: {
      let cloneCart = [...state.cart];
      cloneCart.map((item) => {
        if (item.id == action.payload.id) {
          item.quantity++;
        }
      });
      return { ...state, cart: cloneCart };
    }
    case SUB: {
      let cloneCart = [...state.cart];
      cloneCart.map((item) => {
        if (item.id == action.payload.id) {
          if (item.quantity > 1) {
            item.quantity--;
          } else {
            cloneCart = state.cart.filter((item) => {
              return item.id !== action.payload.id;
            });
          }
        }
      });
      return { ...state, cart: cloneCart };
    }
    case REMOVE: {
      let newCart = state.cart.filter((item) => {
        return item.id !== action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
