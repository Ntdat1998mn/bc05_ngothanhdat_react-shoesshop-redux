import React, { Component } from "react";

export default class Controller extends Component {
  render() {
    return (
      <div
        class="container text-left py-2 col-3 list-group"
        id="list-tab"
        role="tablist"
      >
        <a
          class="border border-dark px-2 list-group-item-action active"
          id="list-home-list"
          data-toggle="list"
          href="#list-home"
          role="tab"
          aria-controls="home"
        >
          Home
        </a>
        <a
          class=" border border-dark px-2 list-group-item-action"
          id="list-shop-list"
          data-toggle="list"
          href="#list-shop"
          role="tab"
          aria-controls="shop"
        >
          Cart
        </a>
      </div>
    );
  }
}
